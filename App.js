import React from 'react';
import {WebView} from 'react-native-webview';

const App = ()=>{
  return(
    <WebView source={{url: 'https://www.thereporterethiopia.com/'}} style={{marginTop: 20}}/>

  );
};
export default App;